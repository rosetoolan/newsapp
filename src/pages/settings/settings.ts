import { Component } from '@angular/core';
import { IonicPage, NavController} from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  settingsForm:FormGroup;
  newName: string;
  newsSource: String;
  newTitle:any;
  newDescription: any;
  myDescription:any;
  newNews: String;
  myName: String;
  myTitle:any;
  city:string;
  state:string;


  constructor(public navCtrl: NavController, private storage: Storage, private formBuilder: FormBuilder, public alertCtrl: AlertController) {

    this.settingsForm = formBuilder.group({
      username: ['', Validators.required],
      newsSource: ['', Validators.required],
      titleFont: ['', Validators.required],
      descriptionFont: ['', Validators.required],
      weatherCity: ['', Validators.required],
      weatherState: ['', Validators.required]

  });

   this.storage.get('location').then((val) =>{
     if(val !=null){
       let location = JSON.parse(val);
       this.city = location.city;
       this.state = location.state;
     }else{
       this.city ='Dublin';
       this.state='D';
     }
   })


  }

  validate(): boolean{
    if(this.settingsForm.valid){
      return true;
    }
  }



  submit(){

    if (!this.validate()){
     let alert = this.alertCtrl.create({
      title: 'Invalid Input',
      subTitle: 'Ensure that you have filled in all values',
      buttons: ['OK']
    });
    alert.present();
    this.navCtrl.push(SettingsPage);
  }else{
    this.navCtrl.push(HomePage);
  }


    console.log("saved");
    this.newsSource = this.newNews;
    this.newNews = "";
    this.storage.set("newsSource", this.newsSource)

    console.log("here");
    this.myName = this.newName;
    this.newName = "";
    this.storage.set("myName", this.myName);

    console.log("save title");
    this.myTitle = this.newTitle;
    this.newTitle = "";
    this.storage.set("myTitle", this.myTitle);

    console.log("save description");
    this.myDescription = this.newDescription;
    this.newDescription = "";
    this.storage.set("myDescription", this.myDescription);

    let location = {
      city: this.city,
      state: this.state
    }

    console.log(location);
    this.storage.set('location', JSON.stringify(location));
   


    }
   
  }

  
 

  

 
