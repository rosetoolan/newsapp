import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WeatherProvider } from '../../providers/weatherprovider/weatherprovider';
import { Storage } from '@ionic/storage';
/**
 * Generated class for the WeatherPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-weather',
  templateUrl: 'weather.html',
})
export class WeatherPage {
	weather:any;
	location:{
		city:string;
		state:string;
	}

  constructor(public navCtrl: NavController, public navParams: NavParams, 
  	private weatherProvider: WeatherProvider, private storage: Storage) {
  }

  ionViewDidLoad(){
    console.log('ionViewDidLoad WeatherPage');
  }

  ionViewWillEnter(){
  	this.storage.get('location').then((val) => {
  		if (val != null){
  			this.location =JSON.parse(val);
  		}else{

  		this.location = {
  		city: 'Dublin',
  		state: 'D'
  			}
  		}
  		this.weatherProvider.getWeather(this.location.city, this.location.state).subscribe(weather =>{
  		this.weather = weather;
  		console.log(weather);
  		console.log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
  		});
  	});
  }
  

  }
  


  	

