import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';
import { NewsProvider } from '../../providers/news-service/news-service';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-news',
  templateUrl: 'news.html'
})
export class NewsPage {

  newsSource:any;
  news:any = [];
  myTitle: any;
  myDescription: any;
  descriptionFont:any;
  titleFont:any;
  titlef:any;


  constructor(public navCtrl:NavController, private ns: NewsProvider, private storage: Storage) {
  }


ionViewWillEnter(){
    this.storage.get("newsSource")
    .then((apiUrl) => {
      this.newsSource = apiUrl;
      console.log(this.newsSource);
     this.getNewsFromSource(this.newsSource)
      })
      
    this.storage.get("myTitle")
      .then((title) => {
        this.myTitle = title;
        }) 
        
        this.storage.get("myDescription")
        .then((Description) => {
            this.myDescription = Description;
            console.log(this.myDescription);
          }) 
    .catch((error) => alert ("Problem acessing local storage") );
 
 
  
}
getNewsFromSource(url){
  this.ns.getNews(this.newsSource).subscribe(data => {
    console.log(data);
    this.news= (data.articles);
  });

}
  getDescriptionFont(){
  var str = "header";
  console.log(str);
  var result = str.fontsize(this.myDescription);
  document.getElementById("header").innerHTML = result;
}
}



