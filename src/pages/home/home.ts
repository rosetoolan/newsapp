import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { QuoteServiceProvider} from '../../providers/quote-service/quote-service';

import { SettingsPage } from '../../pages/settings/settings';
import { NewsPage } from '../../pages/news/news';
import { WeatherPage } from '../../pages/weather/weather';
import { AboutPage } from '../../pages/about/about';
import { ContactPage } from '../../pages/contact/contact';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  quote: string;
  author: string;
  myName: String;
  newsSource: String;
  

  constructor(public navCtrl: NavController, private storage: Storage, private qp:QuoteServiceProvider) {
    

  }
  
  ionViewWillEnter(){
    this.storage.get("myName")
    .then((data) => {
      this.myName = data;
      })
    .catch((error) => alert ("Problem acessing local storage") );



}



  ionViewDidLoad(){
    this.qp.getQuote("http://quotes.rest/qod.json").subscribe(data => {
      this.quote = data.contents.quotes[0].quote;
      this.author = data.contents.quotes[0].author;
    });
  }

  openSettings(){
    this.navCtrl.push(SettingsPage);
  }

  openNews (){
    this.navCtrl.push(NewsPage);
  }

  openWeather (){
    this.navCtrl.push(WeatherPage);
  }
  openAbout(){
    this.navCtrl.push(AboutPage);
  }

  openContact(){
    this.navCtrl.push(ContactPage);
  }




 
}
