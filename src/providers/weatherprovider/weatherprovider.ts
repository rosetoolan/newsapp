import { Injectable } from '@angular/core';
import { Http } from '@angular/http';



import 'rxjs/add/observable/forkJoin';
import 'rxjs/add/operator/map';

@Injectable()
export class WeatherProvider {
  url;
  constructor(
    public http: Http,
  ) {
    console.log('Hello WeatherProvider Provider');
    this.url ='http://api.apixu.com/v1/current.json?key=b69b00e00df540fa970231407181305&q=';

  }

  getWeather(city, state){
  	return this.http.get(this.url + city)
  	.map(res => res.json());
  }
 
}