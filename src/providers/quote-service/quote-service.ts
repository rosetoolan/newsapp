import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';


/*
  Generated class for the QuoteServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class QuoteServiceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello QuoteServiceProvider Provider');
  }

  getQuote(url): Observable<any> {
    return this.http.get(url);
    //return this.http.get("http://quotes.rest/qod.json");
  }
}
