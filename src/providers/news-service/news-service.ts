import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import 'rxjs/Rx';




/*
  Generated class for the NewsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class NewsProvider {

  constructor(public http: Http) {
    console.log('Hello NewsProvider Provider');
  }



  getNews(url):Observable<any> {
    return this.http.get(url).map(obs => obs.json());
  }

  handleError(error) {
    console.error(error);
    return Observable.throw(error.json().error || 'Server error');
}



}












